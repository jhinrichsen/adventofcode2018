package adventofcode2018

import (
	"strings"
)

// Lines separates one string into lines
// From Haskell
func Lines(s string) []string {
	return strings.Split(s, "\n")
}
